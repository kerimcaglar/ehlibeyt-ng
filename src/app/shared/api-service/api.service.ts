import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Subject, ReplaySubject, throwError, Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, map, timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private headers: HttpHeaders;
  private ebApi: any;

  private successSource: Subject<any>;
  private infoSource: Subject<any>;
  private errorSource: Subject<any>;

  get success$() {
    return this.successSource.asObservable();
  }

  get info$() {
    return this.infoSource.asObservable();
  }

  get error$() {
    return this.errorSource.asObservable();
  }

  constructor(private http: HttpClient) {
    this.ebApi = environment.ebApi;
    this.successSource = new ReplaySubject<any>(1);
    this.infoSource = new ReplaySubject<any>(1);
    this.errorSource = new ReplaySubject<any>(1);

    this.headers = new HttpHeaders({
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json',
      Accept: 'application/json'
    });
  }

  private handleError(err: HttpErrorResponse) {
    const code: number = err.status;
    const message: string = err.error.message ? err.error.message : err.statusText;
    const errors: any = err.error.errors ? err.error.errors : [];

    const error: any = { code, message, errors };

    this.errorSource.next(error);

    return throwError(error);
  }

  private handleStatus(body: any) {
    if (body.status === 'success') {
      this.successSource.next(body);
    } else if (body.status === 'info') {
      this.infoSource.next(body);
    }
    return body;
  }

  // noinspection JSAnnotator
  get(path: string, baseUrl: string= this.ebApi.baseURL, params: any = {}): Observable<any> {
    let urlParams: HttpParams = new HttpParams();
    for (const param in params) {
      if (params.hasOwnProperty(param)) {
        urlParams = urlParams.append(param, params[param]);
      }
    }
    console.log(`${baseUrl}/${this.ebApi.version}/${path}`);
    return this.http.get(`${baseUrl}/${this.ebApi.version}/${path}`, { headers: this.headers, params: urlParams })
        .pipe(
            timeout(30000),
            catchError(err => this.handleError(err))
        );
  }

  post(path: string, data: any = {}, baseUrl: string = this.ebApi.baseURL + '/' + this.ebApi.version,
       headers = this.headers): Observable<any> {
    console.log(data);
    return this.http.post(`${baseUrl}/${path}`, JSON.stringify(data), { headers })
        .pipe(
            timeout(30000),
            map(body => this.handleStatus(body)),
            catchError(err => this.handleError(err))
        );
  }

}
