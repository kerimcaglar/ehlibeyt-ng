export class UserModel {
  id: number;
  category: any;
  father: any;
  mother: any;
  queue: number;
  level: number;
  enterCount: number;
  tc: string;
  password: string;
  name: string;
  lastName: string;
  gender: string;
  birthPlace: string;
  birthDate: string;
  deathPlace: string;
  email: string;
  phone: string;
  emailVisible: boolean;
  phoneVisible: boolean;
  emailStatus: boolean;
  smsStatus: boolean;
  photo: string;

  constructor(model?: any) {
    if (!model) {
      return;
    }
    this.id = model.id;
    this.category = model.category;
    this.father = model.father;
    this.mother = model.mother;
    this.queue = model.queue;
    this.level = model.level;
    this.enterCount = model.enterCount;
    this.tc = model.tc;
    this.password = model.password;
    this.name = model.name;
    this.lastName = model.lastName;
    this.gender = model.gender;
    this.birthPlace = model.birthPlace;
    this.birthDate = model.birthDate;
    this.deathPlace = model.deathPlace;
    this.email = model.email;
    this.phone = model.phone;
    this.emailVisible = model.emailvisible;
    this.phoneVisible = model.phoneVisible;
    this.emailStatus = model.emailStatus;
    this.smsStatus = model.smsStatus;
    this.photo = model.photo;
  }

  clear() {
    this.category = null;
    this.father = null;
    this.mother = null;
    this.name = '';
    this.lastName = '';
    this.password = '';
    this.gender = '';
  }

}
