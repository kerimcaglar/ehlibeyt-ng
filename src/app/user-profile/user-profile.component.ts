import { Component, OnInit } from '@angular/core';
import {UserModel} from './user.model';
import {ApiService} from '../shared/api-service/api.service';
import {Select2OptionData} from 'ng-Select2';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {


  users: UserModel;
  loading = false;
  apiSubscription: any;
  errors: any;
  fatherId: any;
  motherId: any;
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  public categoryList: Array<Select2OptionData>;
  categoryListArray = [];

  public manList: Array<Select2OptionData>;
  manListArray = [];

  public womanList: Array<Select2OptionData>;
  womanListArray = [];

  public placeholder = 'Ara...';

  constructor(private apiService: ApiService) {
    this.users = new UserModel();
  }

  ngOnInit() {
    this.getCategories();
    // this.getAllUsers();
    this.getMen();
    this.getWomen();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.manListArray.filter(option => option.toLowerCase().includes(filterValue));
  }

  recordSpouseData() {
    this.loading = true;
    this.apiSubscription = this.apiService.post('create-spouse', {
      husbandId: this.fatherId,
      wifeId: this.motherId
    }).subscribe(json => {
    }, (err) => {
      this.loading = false;
      this.errors = err.errors;
    });
  }

  saveUser() {
    this.loading = true;
    this.apiSubscription = this.apiService.post('create-user', this.users).subscribe(json => {
      this.loading = false;
      this.errors = [];
      this.users.clear();
    }, (err) => {
      this.loading = false;
      this.errors = err.errors;
    });
  }

  getCategories() {
    this.loading = true;

    this.apiSubscription = this.apiService.get('categories').subscribe(response => {
      for (const model of response.data) {
        this.categoryListArray.push({id: model.id, text: model.description});
        this.categoryList = this.categoryListArray;
      }
      this.loading = false;
    }, error => {
      console.log(error);
      this.loading = false;
    });
  }

  /*getAllUsers() {
    this.loading = true;

    this.apiSubscription = this.apiService.get('users').subscribe(response => {
      for (const model of response.data) {
        this.users.push(new UserModel(model));
      }

      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }*/

  getMen() {
    this.loading = true;

    this.apiSubscription = this.apiService.get('man-users').subscribe(response => {
      for (const model of response.data) {
        this.manListArray.push({id: model.id, text: model.name});
        this.manList = this.manListArray;
      }

      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

  getWomen() {
    this.loading = true;

    this.apiSubscription = this.apiService.get('woman-users').subscribe(response => {
      for (const model of response.data) {
        this.womanListArray.push({id: model.id, text: model.name});

        this.womanList = this.womanListArray;
      }
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }


}
